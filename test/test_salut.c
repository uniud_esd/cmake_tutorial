#include <stdio.h>
#include <stdlib.h>
#include "salutation.h"
#include "reverse.h"

int main(int argc, char** argv) {

  char* reversed = reverse(argv[1]);
  char* greeting = salutation(reversed);
  printf("%s",greeting);
  free(greeting);
  free(reversed);

  return 0;
}
