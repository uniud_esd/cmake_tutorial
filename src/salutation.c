#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char* salutation(char* name) {

  char* result;

  result = (char*) malloc(100*sizeof(char));

  strcat(result,"Hello, ");
  strcat(result,name);
  strcat(result,"!\n");

  return result;
}
